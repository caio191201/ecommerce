import { Component, Inject, OnInit } from "@angular/core";
import { inject } from "@angular/core/testing";

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ClienteService } from "../cliente.service";

@Component({
    templateUrl: "./popUp.html",
    styleUrls: ['../app.component.scss']

})
export class PopUp{
    
    constructor(private clientService: ClienteService,private dialog: MatDialog, private dialogRef: MatDialogRef<PopUp>, @Inject(MAT_DIALOG_DATA) private data: any) {

    }
    dadosUser: any = []
    dadoArmazenado: any = [];
    ngOnInit() {
      this.clientService.getFormInfo().subscribe(val => console.log(val));
      this.clientService.getFormInfo().subscribe(data => {
        this.dadoArmazenado = data
        console.log(this.dadoArmazenado)
   })
    }


    
    

}

