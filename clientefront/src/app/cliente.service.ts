import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {


  constructor(private httpClient: HttpClient) { }

  getFormInfo() {
    return this.httpClient.get('http://localhost:3000/posts');
  }
}
