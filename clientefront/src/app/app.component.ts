import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ClienteService } from './cliente.service';
import { PopUp } from './descpopup/popUp';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  constructor(private clienteService: ClienteService, private dialog: MatDialog) {}

  title = 'clientefront';
  prodForCli: any = [];

  ngOnInit() {
    this.clienteService.getFormInfo().subscribe((data) => console.log(data));
    this.clienteService.getFormInfo().subscribe(data => {
      this.prodForCli = data;
    })
  }

  abrirPopUp() {
    const popUpDialog = this.dialog.open(PopUp,{
      height: '55%',
      width: '50%',
      data: {
        text: 'oi'
      }
    })
  }

}
