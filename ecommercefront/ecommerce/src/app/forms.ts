export class Forms {
    constructor(
        public title: string,
        public description: string,
        public value: string,
        public url: string
    ) {}
}
