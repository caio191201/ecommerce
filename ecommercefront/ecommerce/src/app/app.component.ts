import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { FormService } from './form.service';
import { Forms } from './forms';
import { PopUp } from './pastaPopUp/popUp';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private formService: FormService, private dialog: MatDialog) {}

  title = 'ecommerce';
  dadosUser = new Forms('', '', '','');
  dadoArmazenado: any = [];
  
  ngOnInit() {
    this.formService.getFormInfo().subscribe(val => console.log(val));
    this.formService.getFormInfo().subscribe(data => {
      this.dadoArmazenado = data
      console.log(this.dadoArmazenado)
 })
  }

  abrirPopUp() {
    const popUpDialog = this.dialog.open(PopUp,{
      height: '55%',
      width: '50%',
      data: {
        text: 'oi'
      }
    })
    popUpDialog.afterClosed().subscribe((result) => {
      if(result.title && result.description &&result.value && result.url) {
        this.onSubmit(result)
      }
    })
  }
  getData() {
    
  }
  onSubmit(data: any) {
    this.formService.addFormsInfo(data).subscribe((data) => {
      console.log(data)
      this.formService.getFormInfo().subscribe(data => {
        this.dadoArmazenado = data
        console.log(this.dadoArmazenado)
   })
    })
    console.warn(data);
  }
  
}
