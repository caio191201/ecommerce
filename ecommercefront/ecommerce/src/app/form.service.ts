import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppComponent } from './app.component';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(private httpClient: HttpClient) { }

  getFormInfo() {
    return this.httpClient.get('http://localhost:3000/posts');
  }

  addFormsInfo(obj: any) {
    const headers = this.createHeaders();
    return this.httpClient.post('http://localhost:3000/posts', JSON.stringify(obj), {headers});
  }

  //Cria os headers
  createHeaders() {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set("Access-Control-Allow-Origin", "*");
    headers = headers.set("Content-Type", "application/json");
    return headers;
  }
}
