import { Component, Inject } from "@angular/core";
import { inject } from "@angular/core/testing";

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
    templateUrl: "./popUp.html",
    styleUrls: ['../app.component.css']

})
export class PopUp {
    dadosUser: any = {}
    
    constructor(private dialog: MatDialog, private dialogRef: MatDialogRef<PopUp>, @Inject(MAT_DIALOG_DATA) private data) {
        const text = data.text
    }
    enviar() {
        this.dialogRef.close(this.dadosUser)   
    }
    fechar() {
     this.dialogRef.close(this.dadosUser)   
    }

}