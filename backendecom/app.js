const express = require('express');
const mongoose  = require('mongoose');
const app = express();
const bodyParser = require('body-parser');
const cors = require ('cors');
require('dotenv/config');

//Middleware
app.use(cors());
app.use(bodyParser.json());

//Importar rotas
const postsRoute = require('./Rotas/posts');
app.use('/posts', postsRoute);

//Rotas
app.get('/', (req, res) => {
    res.send('Teste');
});

//Posts

//Conectar a base de dados
mongoose.connect(process.env.DB_CONNECTION, 
{ useNewUrlParser: true, useUnifiedTopology: true}, () => console.log('connected to db'));


//Como iniciar
app.listen(3000);