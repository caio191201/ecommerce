const mongoose = require('mongoose');


const FormsItem = mongoose.Schema({
    title: String,
    description: String,
    value: Number,
    url: String
})



module.exports = mongoose.model('Posts', FormsItem);