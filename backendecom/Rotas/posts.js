const express = require('express');
const { db } = require('../models/Posts');
const router = express.Router();
const Post = require('../models/Posts');


//PEGA OS POSTS
router.get('/', async (req, res) => {
    try {
        const posts = await Post.find();
        res.json(posts);
    } catch (err) {
        res.json({message: err});
    }
});

//ENVIA OS POSTS
router.post('/', async (req, res) => {
    //const atividade = req.body.title;
    //console.log(atividade);
    const tituloForm = req.body.title;
    const descriptForm = req.body.description;
    const valueProd = req.body.value;
    const urlProd = req.body.url;
    const post = new Post({
        title: tituloForm,
        description: descriptForm,
        value: valueProd,
        url: urlProd
   });

    try {
        const savedPost = await post.save();
        res.json(savedPost);
    } catch(err) {
        res.json({ message: err });
    }
});

//PEGA UM POST ESPECIFICO
router.get('/:postID', async (req, res) => {
    try {
        const post = await Post.findById(req.params.postID);
        res.send(post);
    } catch(err) {
        res.json({ message: err });
    }
    
})

//DELETAR UM POST
router.delete('/:postID', async (req, res) => {
    try {
        //const idItemLista = req.params.postID;
        const removedPost = await Post.remove({_id: req.params.postID});
        res.json(removedPost);
    } catch(err) {
        res.json({ message: err });
    }
})

//ATUALIZAR UM POST
router.patch('/:postID', async (req, res) => {
    try {
        const attPost = await Post.updateOne({_id: req.params.postID}, {$set: {title: req.body.title, description: req.body.description, value: req.body.value, url:req.body.url}} );
        res.json(attPost);
    } catch(err) {
        res.json({ message: err });
    }
})

module.exports = router;
